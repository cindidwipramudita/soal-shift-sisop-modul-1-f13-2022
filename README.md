# soal-shift-sisop-modul-1-f13-2022



## Anggota Kelompok 
1. Shaula Aljauhara Riyadi 5025201265
2. Cindi Dwi Pramudita 5025201042
3. Rachel Anggieuli AP 5025201263

## Soal 1

Diminta untuk membuat suatu program dengan sistem register dan login, untuk passwordnya memiliki 4 ketentuan yang harus dilaksanakan terlebih dahulu sebelum dapat digunakan yaitu lebih dari sama dengan 8, berupa alphanumeric, tidak boleh sama dengan username dan gabungan berupa huruf kapital dan huruf kecil.  

### 1.a: 
Untuk register dibuat pada script register.sh, dan jika sudah berhasil register maka akan disimpan di file user.txt.

    'echo $username" "$password >> ./users/user.txt'

### 1.b
membuat password dengan requirement
i. Minimal 8 karakter
ii. Memiliki minimal 1 huruf kapital dan 1 huruf kecil
iii. Alphanumeric
iv. Tidak boleh sama dengan username
Menggunakan -s agar password tidak terlihat atau hidden jika sedang ditulis.

    'while [ $oke -lt 4 ]
    do
	oke=0

	echo -n "Password : "
	read -s password
	echo " "

	if [ ${#password} -ge 8 ]
	then
		oke=$((oke+1))
	else
		echo "- Password minimal 8 karakter!"
	fi

	if [[ "$password" =~ [A-Z] ]] && [[ "$password" =~ [a-z] ]]
	then
		oke=$((oke+1))
	else
		echo "- Password memiliki minimal 1 huruf kapital dan 1 huruf kecil"
	fi

	if [[ "$password" =~ ^[a-zA-Z0-9]*$ ]]
	then
		oke=$((oke+1))
	else
		echo "- Password hanya berupa Alphanumeric(a-z, A-Z, 0-9)"
	fi

	if [ $password != $username ]
	then 
		oke=$((oke+1))
	else
		echo "- Password tidak boleh sama dengan username!"
	fi

    echo " "
    done'

Menggunakan -s agar password tidak terlihat atau hidden jika sedang ditulis.

### 1.c
: Menyimpan hasil login dan register ke log.txt
## 1.c.i 
mengecek pada user2.txt apakah username yang akan di register sudah terdaftar di sistem 

    while [ $i -lt 1 ]
    do
    echo -n "username : "
    read username

    if [[ ${user[*]} =~ "$username" ]]
    then
    echo $waktu "REGISTER: ERROR User already exists" >> log.txt
    echo -e "Username telah digunakan!\n\n"
    else
    i=$((i+1))
    fi
    done

## 1.c.ii 
Jika register telah berhasil, maka akan mengeluarkan message pada log.txt 
    echo $t "REGISTER: INFO User" $username "registered successfully" >> log.txt
    echo -e "Register Berhasil!\n"

## Soal 2

Pada soal 2, ditugaskan untuk membaca log website https://daffainfo.info dan membuat sebuah script awk yang dapat melaksakan berbagai tugas. Hasil dari tugas tersebut disimpan dalam file bernama ratarata.txt untuk poin B dan result.txt untuk poin C, D, dan E.

### Poin A: Membuat folder forensic_log_website_daffainfo_log

Directory dapat dibuat dengan menggunakan command

    `mkdir forensic_log_website_daffainfo_log`

### Poin B: Menghitung rata-rata request per jam pada website

Langkah pertama yaitu menghitung setiap request per jam dimulai dari jam 00 hingga 12. Kemudian hasil request per jam tersebut disimpan dalam count untuk mentotal semua request per jam menggunakan loop. Setelah itu didapatkan count sebanyak 973 request dan didapatkan total sebanyak 13. Akan tetapi, diketahui bahwa request dimulai dari jam 00 hingga 12 maka hasil total yaitu 13 dikurangi 1, sehingga jika 973 dibagi dengan 12 jam didapatkan 81.0833 request per jam.

    `
    awk -F: 'NR == 1 { min = max = $3 }
    { if ($3 > max) max = $3; else if ($3 < min) min = $3 }
    END {print "Rata-rata serangan adalah sebanyak", (NR-1)/(max-min+1), "request per jam\n"}
    `

Selanjutnya hasil perhitungan tersebut disimpan dalam file ratarata.txt menggunakan

    `log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt`

### Poin C: Menampilkan IP yang paling banyak melakukan request ke server dan jumlah requestnya

Langkah pertama yaitu menghitung IP dengan request terbanyak yang kemudian disimpan dalam max. Setelah itu ditampilkan IP dengan request terbanyak berserta jumlah requestnya dengan loop. Didapatkan IP 216.93.144.47 mengakses paling banyak ke server dengan jumlah request total 539.

    `
    awk -F: 'NR>1{ gsub(/"/, "") }
    { if ( a[$1]++ >= max ) max = a[$1] }
    END { for (i in a) if(max == a[i]) print "IP yang paling banyak mengakses server adalah:", i, "sebanyak", a[i], "requests\n" }
    print"IP yang paling banyak mengakses server adalah:",i,"sebanyak",IP[i],"requests\n"}
    `

Selanjutnya hasil tersebut disimpan dalam file result.txt menggunakan

`log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt`

### Poin D: Menampilkan jumlah request dengan user-agent curl

Karena kata curl spesifik dan berbeda dengan yang lain, maka dapat dicari menggunakan command /curl/ dan jika menemukan maka akan ditambah terus menerus. Sehingga didapatkan sebanyak 58 request yang menggunakan curl sebagai user-agent.

    `
    awk -F: ' /curl/ {count++}
    END{print"Ada",count,"request yang menggunakan curl sebagai user-agent\n"}
    `

Selanjutnya hasil tersebut disimpan dalam file result.txt menggunakan

`log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt`

### Poin E: Menampilkan IP yang mengakses pada jam 2 pagi

Karena yang dicari adalah jam maka dapat dispesifikkan yang dicari pada kolom 3 dan difilter 02 karena jam 2 pagi. Sehingga dapat ditampilkan bahwa IP 128.14.133.58, 109.237.103.38, 88.80.186.144, 2.228.115.154, 145.239.154.84, 194.126.224.140.

    `
    awk -F: 'NR>1{ gsub(/"/, "")}
    { if ( $3 ~ /02/ && $2 ~ /^22/ )  a[$1]++ }
    END { for (i in a) print i, "Jam 2 pagi" }
    `

Selanjutnya hasil tersebut disimpan dalam file result.txt menggunakan

`log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt`

Kendala
Selama mengerjakan masih terdapat beberapa syntax asing yang masih belum terbiasa, sehingga pengerjaan pun tidak maksimal.

## soal 3
pada soal 3,mahasiswa disuruh untuk menampilkan berapa memory yang digunakan dan menunjukkan size dari
suatu directory dan menampilkan data monitor tersebut tiap menit dalam file minute_log.sh dan data permenit
tersebut akan disimpan per jam dalam file aggregate_minutes_to_hourly_log.sh

### poin A membuat file minute_log.sh
pertama-tama membuat directory log dengan command

`mkdir log`

kemudian mengambil data resource ram dan memasukkan data tersebut dalam file sementara menggunakan command

`free -m /home/dioz > temp.txt`
`echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,   path_size" > text1.txt`

lalu stream file akan print stream file sementara tersebut menggunakan dua file,yaitu file text1 dan file 
text2 yang berfungsi untuk mengakumulasi setiap data agar dapat memudahkan kita dalam mengambil data yang diperlukan menggunakan command

`cat temp.txt | awk '{ORS=""} FNR == 2 {print $2 "," $3 "," $4 "," $5 "," $6 "," $7 ","}' >> text1.txt
cat temp.txt | awk '{ORS=""} FNR == 2 {print $2 "," $3 "," $4 "," $5 "," $6 "," $7 ","}' > text2.txt`

File text1 dibuat untuk memudahkan kita dalam menyusun data seperti format yang harus diikuti.
Setelah itu, tambahkan nama path dengan menggunakan command

`echo -n `pwd`"," >> text1.txt
echo -n `pwd`"," >> text2.txt`


Untuk menambahkan resource Disk,  dapat menggunakan command
du -sh | awk '{print $1}' >> text1.txt du -sh | awk '{print $1}' >> text2.txt
Karena untuk isi file text1.txt dan text2.txt sudah sesuai dengan format yang diinginkan, sekarang data tersebut akan dimasukkan kedalam file metrics dengan command

`cat text1.txt >> log/metrics_"`date +"%Y%m%d%H%M%S"`".log
cat text2.txt >> log/metrics_hourly_"`date +"%Y%m%d%H"`".log`

Setelah file metrics berhasil dibuat, atur permissionnya agar hanya bisa diakses oleh user saja dengan command

`chmod 700 log/metrics_"`date +"%Y%m%d%H%M%S"`".log
chmod 700 log/metrics_hourly_"`date +"%Y%m%d%H"`".log`

### poin B membuat file aggregate_minutes_to_hourly_log.sh

Untuk script ini, masukkan terlebih dahulu format isi file log ke dalam file metrics dengan command

`echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" > log/metrics_agg_"`date +"%Y%m%d%H"`".log`

Dengan file hourly_log sementara yang telah dibuat sebelumnya,akan diambil tiga nilai yang telah diolah yaitu minimum, maksimum, dan rata-rata dari tiap jenis data yang sama dan dimasukkan ke dalam file aggregate dengan format metrics_agg_{YmdH} dengan command sebagai berikut :

nilai minimum 

`echo -n "minimum," >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log | awk -F, '
BEGIN {var1=15000; var2=15000; var3=15000; var4=15000; var5=15000; var6=15000; var7=15000; var8=15000; var9=15000}
{if($1<var1) var1=$1;}
{if($2<var2) var2=$2;}
{if($3<var3) var3=$3;}
{if($4<var4) var4=$4;}
{if($5<var5) var5=$5;}
{if($6<var6) var6=$6;}
{if($7<var7) var7=$7;}
{if($8<var8) var8=$8;}
{if($9<var9) var9=$9;}
END {print var1","var2","var3","var4","var5","var6","var7","var8","var9}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log`

nilai maximum 

`echo -n "maximum," >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log  | awk -F, '
BEGIN {var1=0; var2=0; var3=0; var4=0; var5=0; var6=0; var7=0; var8=0; var9=0}
{if($1>var1) var1=$1;}
{if($2>var2) var2=$2;}
{if($3>var3) var3=$3;}
{if($4>var4) var4=$4;}
{if($5>var5) var5=$5;}
{if($6>var6) var6=$6;}
{if($7>var7) var7=$7;}
{if($8>var8) var8=$8;}
{if($9>var9) var9=$9;}
END {print var1","var2","var3","var4","var5","var6","var7","var8","var9}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log`

nilai rata-rata

`echo -n "average,"  >> log/metrics_agg_"`date +"%Y%m%d%H"`".log
cat log/metrics_hourly_"`date +"%Y%m%d%H"`".log  | awk -F, '
BEGIN {var1=0; var2=0; var3=0; var4=0; var5=0; var6=0; var7=0; var8=0; var9=0}
{var1+=$1;}
{var2+=$2;}
{var3+=$3;}
{var4+=$4;}
{var5+=$5;}
{var6+=$6;}
{var7+=$7;}
{var8+=$8;}
{var9+=$9;}
END {print var1/60","var2/60","var3/60","var4/60","var5/60","var6/60","var7/60","var8/60","var9/60}' >> log/metrics_agg_"`date +"%Y%m%d%H"`".log`

setelah membuat script diatas,lakukan instalasi crontab dengan menggunakan cronjob

`* * * * * bash minute_log.sh
59 * * * * bash aggregate_minutes_to_hourly_log.sh`

sama seperti file sebelumnya,ubah permission untuk file ini sehingga dapat dilihat oleh user saja

`chmod 700 log/metrics_agg_"`date +"%Y%m%d%H"`".log`

## kendala
selama pratikum,saya kebingungan dengan syntax asing dan mekanisme baru sehingga saya cukup kesusahan saat mengerjakan pratikum yang diberikan
