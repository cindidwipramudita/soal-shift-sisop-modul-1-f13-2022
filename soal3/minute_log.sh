#!/bin/bash
echo "`"date"`"

#ram usage
ram_usage() {
mem_free = "`"free -m"`"
echo "$mem_free"
}

#size_directory
size_directory() {
size_drcty = "`"du -sh /home/{user}/ | sort -rh "`"
echo "$size_drcty"
}

ram_usage
size_directory

