#!/bin/bash

# Buat folder forensic_log_website_daffainfo_log
#mkdir forensic_log_website_daffainfo_log

# Average requests per jam
awk -F: '
NR == 1 { min = max = $3 }
{ if ($3 > max) max = $3; else if ($3 < min) min = $3 }
END {print "Rata-rata serangan adalah sebanyak", (NR-1)/(max-min+1), "request per jam\n"} 
'
# log_website_daffainfo.log >> forensic_log_website_daffainfo_log/ratarata.txt


# IP yang paling banyak melakukan request ke server 
awk -F: '
NR>1 { gsub(/"/, "") } 
{ if ( a[$1]++ >= max ) max = a[$1] } 
END { for (i in a) if(max == a[i]) print "IP yang paling banyak mengakses server adalah:", i, "sebanyak", a[i], "requests\n" } 
'
# log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

# Banyaknya request yang menggunakan curl sebagai user-agent
awk -F: '
$9 ~ /curl/ {x++} 
END { print "Ada", x, "requests yang menggunakan curl sebagai user-agent\n" }
'
# log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

# IP yang mengakses pada Jam 2 Pagi tanggal 22
awk -F: ' 
NR>1 { gsub(/"/, "") } 
{ if ( $3 ~ /02/ && $2 ~ /^22/ )  a[$1]++ }
END { for (i in a) print i, "Jam 2 pagi" }
'
# log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

